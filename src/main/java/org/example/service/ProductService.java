package org.example.service;

import com.example.productapp.dto.request.RequestProductDTO;
import com.example.productapp.dto.response.ResponseProductDTO;
import org.example.util.RestClient;
import org.springframework.stereotype.Service;

@Service
public class ProductService {

    private RestClient _restClient;

    public ProductService() {
        _restClient = new RestClient<>();
    }

    public ResponseProductDTO findById(int id) {
        return (ResponseProductDTO) _restClient.get("/api/product/" + id, ResponseProductDTO.class);
    }

    public ResponseProductDTO update(int id, int newStock) {
        return (ResponseProductDTO) _restClient.patch("/api/product/" + id + "/stock/" + newStock, ResponseProductDTO.class);
    }

    public ResponseProductDTO add(RequestProductDTO dto) {
        return (ResponseProductDTO) _restClient.post("/api/product", dto, ResponseProductDTO.class);
    }

}
